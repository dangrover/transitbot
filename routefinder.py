# -*- coding: utf-8 -*-

import os
import transitfeed
from pprint import pprint
import datetime
from geopy.distance import distance
from fuzzywuzzy import fuzz

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
CALTRAIN_GTFS_PATH = os.path.join(SCRIPT_DIR, "data", "caltrain2014")
CALTRAIN_SCHEDULE = transitfeed.schedule.Schedule()
CALTRAIN_SCHEDULE.Load(CALTRAIN_GTFS_PATH)


def find_stop_from_loc(location_coords):
    def distance_from_loc(stop_info, coords):
        stop_location = (stop_info.stop_lat, stop_info.stop_lon)
        return distance(stop_location, coords)

    closest = sorted(CALTRAIN_SCHEDULE.GetStopList(),
                     key=lambda stop_info: distance_from_loc(stop_info,location_coords))

    return closest[0]

def find_stop_from_name(str_input):
    closest = sorted(CALTRAIN_SCHEDULE.GetStopList(),
    key=lambda stop: fuzz.ratio(sanitize_stop_name(stop.stop_name), str_input),
    reverse=True)

    return closest[0]

#    stops = filer(lambda stop_info: stop.GetLongName().contains

def find_suitable_trip_segment(trip, origin_station, dest_station, depart_after_time):
    depart_after_seconds = (depart_after_time - datetime.datetime.min).seconds
    stoptimes = trip.GetStopTimes()
    start_idx = None

    #print "Finding suitable segments for %s -> %s" % (origin_station.stop_name, dest_station.stop_name)

    for i in range(0,len(stoptimes)):
        if (stoptimes[i].stop.stop_id == origin_station.stop_id) and (stoptimes[i].departure_secs >= depart_after_seconds):

            start_idx = i
            break

    if start_idx != None:
        end_idx = None
        for i in range(start_idx, len(stoptimes)):
            if stoptimes[i].stop.stop_id == dest_station.stop_id and stoptimes[i].arrival_secs > stoptimes[start_idx].departure_secs:
                end_idx = i
                break

        if end_idx:
            return range(start_idx, end_idx+1)

    return None

def find_suitable_trips(origin_station, dest_station, depart_after_time):
    trip_list = CALTRAIN_SCHEDULE.GetTripList()

    return filter(lambda t: find_suitable_trip_segment(t, origin_station, dest_station, depart_after_time) != None, trip_list)

def sanitize_stop_name(stop_name):
    return stop_name.replace("Caltrain Station","").strip()

def format_trip_segment(trip, segment_range):
    #print "Formatting trip segment with range %s, that's %d->%d" % (segment_range, segment_range[0], segment_range[-1])
    times = trip.GetStopTimes()
    origin_stop_time = times[segment_range[0]]
    dest_stop_time = times[segment_range[-1]]

    return u"%s (%s) 🚆 %s (%s)" % (sanitize_stop_name(origin_stop_time.stop.stop_name),
    format_trip_seconds(origin_stop_time.departure_secs),
    sanitize_stop_name(dest_stop_time.stop.stop_name),
    format_trip_seconds(dest_stop_time.departure_secs))

def format_trip_seconds(seconds_after_midnight):
    dt = datetime.datetime.combine(datetime.date.today(), datetime.time.min) + datetime.timedelta(seconds=seconds_after_midnight)
    return dt.strftime("%I:%m%p").lower()

def trip_arrival_secs(trip, dest_stop_id):
    arrival_stop = filter(lambda st: st.stop_id == dest_stop_id, trip.GetStopTimes())[0]
    return arrival_stop.arrival_secs

if __name__ == "__main__":
    sf_stop = find_stop_from_loc((37.7749,-122.4194))
    #print "Nearest stop to SF is %s" % sf_stop
    #sunnyvale_stop = find_stop_from_loc((37.3688,-122.0363))
    #print "Nearest stop to Sunnyvale is %s" % sunnyvale_stop
    sunnyvale_stop = find_stop_from_name("Sunnyvale")
    print "Sunnyvale stop = %s" % sunnyvale_stop

    depart_after = datetime.datetime.now() #+ datetime.timedelta(hours=24)
    trips = find_suitable_trips(origin_station=sf_stop,
                        dest_station=sunnyvale_stop,
                        depart_after_time=depart_after)

    best_trips = sorted(trips,key=lambda t: trip_arrival_secs(t, sunnyvale_stop.stop_id))
    best_trips = best_trips[0:min(len(best_trips)-1, 2)]

    print "\nBest 2 trips are:"
    for t in best_trips:
        seg = find_suitable_trip_segment(t, sf_stop, sunnyvale_stop, depart_after)
        print format_trip_segment(t, seg)
