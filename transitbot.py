import os, sys
import routefinder
import requests
import json
import logging
import datetime
from httplib import HTTPConnection
from flask import Flask, request, session, g, redirect, url_for, abort, Response, make_response
from wit import Wit
import pickledb
from pprint import pprint

from routefinder import find_stop_from_name, find_suitable_trip_segment, format_trip_segment, find_suitable_trips, sanitize_stop_name, trip_arrival_secs

from requests.packages.urllib3.exceptions import SNIMissingWarning, InsecurePlatformWarning
requests.packages.urllib3.disable_warnings(SNIMissingWarning)
requests.packages.urllib3.disable_warnings(InsecurePlatformWarning)

def wit_getnexttrip(request, wit_interpretation):
    print "Getting next trip for request..."
    pprint(request)

    print "Wit interpretation is:"
    pprint(wit_interpretation)

    from_loc_name = None
    to_loc_name = None

    try:
        if wit_interpretation['entities'].get('location') and len(wit_interpretation['entities'].get('location')) == 2:
            from_loc_name = wit_interpretation['entities']['location'][0]['value']
            to_loc_name = wit_interpretation['entities']['location'][1]['value']
        else:
            from_loc_name = wit_interpretation['entities']['origin'][0]['value']
            to_loc_name = wit_interpretation['entities']['destination'][0]['value']
    except Exception as e:
        print "Exception occurred trying to find locations: %s" % e
        send_text_message("Sorry, I don't understand where you're trying to go.", request['sender']['id'])
        return

    from_stop = find_stop_from_name(from_loc_name)
    to_stop = find_stop_from_name(to_loc_name)

    depart_after = datetime.datetime.now() #+ datetime.timedelta(hours=24)
    trips = find_suitable_trips(origin_station=from_stop,
                        dest_station=to_stop,
                        depart_after_time=datetime.datetime.now())

    best_trips = sorted(trips,key=lambda t: trip_arrival_secs(t, to_stop.stop_id))

    if len(best_trips) > 0:
        best = best_trips[0]
        seg = find_suitable_trip_segment(best, from_stop, to_stop, depart_after)
        response = format_trip_segment(best, seg)
        send_text_message(response, request['sender']['id'])

        #best_trips = best_trips[0:min(len(best_trips)-1, 2)]

        ##response = "It looks like your best bets are:\n"
        #for t in best_trips:
        #    seg = find_suitable_trip_segment(t, from_stop, to_stop, depart_after)
        #    response += format_trip_segment(t, seg)+"\n"

        #send_text_message(response,request['sender']['id'])
    else:
        response = "Sorry, couldn't find any trains from %s to %s." % (sanitize_stop_name(from_stop), sanitize_stop_name(to_stop))
        send_text_message(response,request['sender']['id'])

def wit_send(request, response):
    print "Wit sending %s / %s" % (request, response)

# paths/constants
FB_PAGE_ACCESS_TOKEN = "EAATomlzt6p0BAIWAMqLBiN0RPsGOZCFWzp2aaqw8ZApWiR5VGLRV0y3uhRXQK8xsiZAeSUVZCN04j5L1nZAkmd9jVXVoehPy4lVPisBc0cLJn1eVidYQqPb5QRzrdiQ2uUmvSZA0rARlcjwUBEqJsJ7uxSUgdiptmZBAZBpt9lBZBTwZDZD"
WIT_SERVER_ACCESS_TOKEN = "MBC47C4FQUV7BVR52JNSYXV27VY33DIG"

VERIFY_TOKEN = 'sftransitbotverify'


# set up logging
#from httplib import HTTPConnection
#HTTPConnection.debuglevel = 1
#logging.basicConfig() # you need to initialize logging, otherwise you will not see anything from requests
#logging.getLogger().setLevel(logging.DEBUG)
#requests_log = logging.getLogger("requests.packages.urllib3")
#requests_log.setLevel(logging.DEBUG)
#requests_log.propagate = True


# wit
wit_actions = {'getnexttrip':wit_getnexttrip, 'send':wit_send}
wit_client = Wit(access_token=WIT_SERVER_ACCESS_TOKEN, actions=wit_actions)

# session store
session_store = pickledb.load("/home/dangrover/sessions/transitbot.db", False)

# app
app = Flask(__name__)
app.config.from_object(__name__)


@app.route('/')
def hello_world():
    return 'Transit Bot is here!'

@app.route('/messenger-webhook',methods=['GET', 'POST'])
def messenger_webhook():
    print "-----------------------------------------------------------"
    print "NEW REQUEST"
    pprint(request)

    if request.args.get('hub.mode') == 'subscribe':
        return subscribe_webhook()

    # parse request body
    data = request.get_json(silent=True)
    pprint(data)

    if data:
        if data.get('object') == 'page':
            for e in data.get('entry'):
                #print "processing event: %s" % e
                for m in e.get('messaging'):
                    if m.get('optin'):
                        process_optin(m)
                    elif m.get('message'):
                        process_message(m)
                    elif m.get('delivery'):
                        process_delivery(m)
                    elif m.get('postback'):
                        process_postback(m)
                    elif m.get('read'):
                        pass # message was read
                    else:
                        return make_response("Invalid event recieved",500)

    return "Webhook is working"

def subscribe_webhook():
    if request.args.get('hub.verify_token') == VERIFY_TOKEN:
        challenge = request.args.get('hub.challenge')
        return challenge
    else:
        return make_response("Invalid verify token!", 401)

def process_delivery(event):
    pass

def session_id_for_user(user_id):
    return "%s-42" % user_id

def process_message(event):
    if event['message'].get('text') == None:
        print "Got a non-text message."
        return
    elif event['message'].get('is_echo') == True:
        print "Skipping echo"
        return

    msg_text = event['message']['text']
    sender_id = event['sender']['id']
    print "Got text msg: '%s'" % event['message']['text']

    #old_context = pickledb.get("%s-session" % sender_id)
    #if old_context == None:
    #    old_context = {}
    context = {}

    # Keep calling wit.converse until we are told to stop
    should_stop = False
    first_call = True
    while should_stop == False:

        wit_response = None

        if first_call:
            wit_response = wit_client.converse(session_id_for_user(sender_id), msg_text, context, verbose=True)
            first_call = False
        else:
            wit_response = wit_client.converse(session_id_for_user(sender_id), None, context, verbose=True)

        if wit_response['type'] == 'merge':
            print "Merge?"

        elif wit_response['type'] == 'msg':
            print "Sending wit's text response: %s" % wit_response['msg']
            send_text_message(wit_response['msg'], sender_id)

        elif wit_response['type'] == "error":
            send_text_message("Sorry, I don't understand. Try using both hands to type.", sender_id)

        elif wit_response['type'] == "action":
            the_action = wit_actions.get(wit_response['action'])
            print "Performing action '%s'" % wit_response['action']
            the_action(event, wit_response)

        elif wit_response['type'] == "stop":
            print "Told to stop"
            should_stop = True
        else:
            print "Unknown response type '%s'!" % wit_response['type']

    #send_text_message("Roger over and out", event['sender']['id'])


def process_optin(event):
    pass

def process_postback(event):
    pass

def send_text_message(text, user_id):
    call_send_api({"recipient":{"id":user_id},
                   "message":{"text":text}})

def call_send_api(message_data):
    result = requests.post("https://graph.facebook.com/v2.6/me/messages",
                           params={"access_token": FB_PAGE_ACCESS_TOKEN},
                           json=message_data)
    print "Send result: %s" % result.text
