import os, sys, json, logging, datetime
from httplib import HTTPConnection
from flask import Flask, request, session, g, redirect, url_for, abort, Response, make_response
import pickledb
from pprint import pprint

import requests
from requests.packages.urllib3.exceptions import SNIMissingWarning, InsecurePlatformWarning
requests.packages.urllib3.disable_warnings(SNIMissingWarning)
requests.packages.urllib3.disable_warnings(InsecurePlatformWarning)

# paths/constants
FB_PAGE_ACCESS_TOKEN = "EAATomlzt6p0BAIWAMqLBiN0RPsGOZCFWzp2aaqw8ZApWiR5VGLRV0y3uhRXQK8xsiZAeSUVZCN04j5L1nZAkmd9jVXVoehPy4lVPisBc0cLJn1eVidYQqPb5QRzrdiQ2uUmvSZA0rARlcjwUBEqJsJ7uxSUgdiptmZBAZBpt9lBZBTwZDZD"

VERIFY_TOKEN = 'testbotverify'


# set up logging
#from httplib import HTTPConnection
#HTTPConnection.debuglevel = 1
#logging.basicConfig() # you need to initialize logging, otherwise you will not see anything from requests
#logging.getLogger().setLevel(logging.DEBUG)
#requests_log = logging.getLogger("requests.packages.urllib3")
#requests_log.setLevel(logging.DEBUG)
#requests_log.propagate = True

# session store
session_store = pickledb.load("/home/dangrover/sessions/testbot.db", False)

# app
app = Flask(__name__)
app.config.from_object(__name__)


@app.route('/')
def hello_world():
    return 'Test Bot is here!!'

@app.route('/messenger-webhook',methods=['GET', 'POST'])
def messenger_webhook():
    print "-----------------------------------------------------------"
    print "NEW REQUEST"
    pprint(request)

    if request.args.get('hub.mode') == 'subscribe':
        return subscribe_webhook()

    # parse request body
    data = request.get_json(silent=True)
    pprint(data)

    if data:
        if data.get('object') == 'page':
            for e in data.get('entry'):
                #print "processing event: %s" % e
                for m in e.get('messaging'):
                    if m.get('optin'):
                        process_optin(m)
                    elif m.get('message'):
                        process_message(m)
                    elif m.get('delivery'):
                        process_delivery(m)
                    elif m.get('postback'):
                        process_postback(m)
                    elif m.get('read'):
                        pass # message was read
                    else:
                        return make_response("Invalid event recieved",500)

    return "Webhook is working"

def subscribe_webhook():
    if request.args.get('hub.verify_token') == VERIFY_TOKEN:
        challenge = request.args.get('hub.challenge')
        return challenge
    else:
        return make_response("Invalid verify token!", 401)

def process_delivery(event):
    pass

def session_id_for_user(user_id):
    return "%s-42" % user_id

def process_message(event):
    if event['message'].get('text') == None:
        print "Got a non-text message."
        return
    elif event['message'].get('is_echo') == True:
        print "Skipping echo"
        return

    msg_text = event['message']['text']
    sender_id = event['sender']['id']
    print "Got text msg: '%s'" % event['message']['text']

    send_text_message("I'm here", sender_id)

def process_optin(event):
    pass

def process_postback(event):
    pass

def send_text_message(text, user_id):
    call_send_api({"recipient":{"id":user_id},
                   "message":{"text":text}})

def call_send_api(message_data):
    result = requests.post("https://graph.facebook.com/v2.6/me/messages",
                           params={"access_token": FB_PAGE_ACCESS_TOKEN},
                           json=message_data)
    print "Send result: %s" % result.text